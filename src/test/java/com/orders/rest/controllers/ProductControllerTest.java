package com.orders.rest.controllers;

import com.orders.domain.Order;
import com.orders.domain.Product;
import com.orders.domain.Vendor;
import com.orders.repositories.OrderMemoryRepository;
import com.orders.repositories.ProductMemoryRepository;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by jan on 25/11/14.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductControllerTest {

    private static Logger LOG = LoggerFactory.getLogger(ProductControllerTest.class);

    MockMvc mockMvc;

    @Mock
    ProductMemoryRepository productRepository;

    @InjectMocks
    ProductController controller;

    UUID key = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
    }

    @Test
    public void thatViewProductRendersCorrectly() throws Exception {

        LOG.info("START thatViewProductRendersCorrectly");

        Map<UUID, Product> products = new HashMap<UUID, Product>();
        products.put(key, new Product(key, "productForTest", new Vendor("vendor1")));

        when(productRepository.findById(key)).thenReturn(products.get(key));

        this.mockMvc.perform(
                get("/api/products/{id}", key.toString())
                        .accept("application/hal+json"))
                .andExpect(jsonPath("$.identifier").value(key.toString()));
    }
}
