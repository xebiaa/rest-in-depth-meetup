package com.orders.rest.controllers;



import com.fasterxml.jackson.databind.ObjectMapper;
import com.orders.Application;
import com.orders.domain.Order;
import com.orders.domain.OrderItem;
import com.orders.domain.Product;
import com.orders.domain.Vendor;
import com.orders.repositories.OrderMemoryRepository;
import com.orders.repositories.ProductMemoryRepository;
import com.orders.rest.resources.NewOrderResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by marco on 07/11/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {Application.class})
public class OrderControllerTest {

    @Autowired
    WebApplicationContext wac;

    @Autowired
    ProductMemoryRepository productRepository;

    @Autowired
    OrderMemoryRepository orderRepository;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void thatOrdersCanBeQueried() throws Exception {

        Product product = new Product(UUID.randomUUID(),"product 1", new Vendor("vendor1"));
        productRepository.save(product);
        Order order = new Order(UUID.randomUUID(),"testUser2");
        order.setOrderItem(new OrderItem(new Integer("42"), product.getIdentifier(), order.getId()));
        orderRepository.save(order);


        this.mockMvc.perform(
                get("/api/orders")
                        .accept("application/hal+json"))
                .andDo(print())
                .andExpect(header().string("Content-Type", "application/hal+json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].order.accountId").exists());

    }

    @Test
    public void thatOrderCanBeEntered() throws Exception {

        NewOrderResource res = new NewOrderResource(new Integer("2"), UUID.randomUUID());
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(res);
        MvcResult result = this.mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$._links").exists())
                .andExpect(jsonPath("$.order.accountId").exists()).andReturn();
        String etag = result.getResponse().getHeader("ETag").toString();
        Assert.assertNotNull(etag);
    }

    @Test
    public void thatOrderCanNotBeUpdated() throws Exception {

        NewOrderResource res = new NewOrderResource(new Integer("5"), UUID.randomUUID());
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(res);
        this.mockMvc.perform(put("/api/orders").contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(print())
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void thatOrderCanBeDeleted() throws Exception {

        Product product = new Product(UUID.randomUUID(),"product 2", new Vendor("vendor1"));
        productRepository.save(product);
        Order order = new Order(UUID.randomUUID(),"testUser2");
        order.setOrderItem(new OrderItem(new Integer("42"), product.getIdentifier(), order.getId()));
        orderRepository.save(order);

        this.mockMvc.perform(delete("/api/orders/"+order.getId().toString()).header("If-Match", new Integer(orderRepository.findById(order.getId()).hashCode()).toString()))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void thatOrderCanBeApproved() throws Exception {

        Product product = new Product(UUID.randomUUID(),"product 3", new Vendor("vendor1"));
        productRepository.save(product);
        Order order = new Order(UUID.randomUUID(),"testUser3");
        order.setOrderItem(new OrderItem(new Integer("42"), product.getIdentifier(), order.getId()));
        orderRepository.save(order);

        this.mockMvc.perform(put("/api/orders/" + order.getId().toString() + "/approve").header("If-Match", new Integer(orderRepository.findById(order.getId()).hashCode()).toString()))
                .andDo(print())
                .andExpect(status().isAccepted());
    }

}
