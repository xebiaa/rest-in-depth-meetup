package com.orders.rest.controllers;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by jan on 07/11/21.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteMapControllerTest {

    private static Logger LOG = LoggerFactory.getLogger(SiteMapControllerTest.class);

    MockMvc mockMvc;

    @InjectMocks
    SiteMapController controller;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
    }


    @Test
    public void thatSitemapIsReturned() throws Exception {

        this.mockMvc.perform(
                get("/api"))
                .andExpect(MockMvcResultMatchers.content().contentType("application/hal+json"))
                .andExpect(jsonPath("$.sitemap", containsString("This is the sitemap")));
    }
}
