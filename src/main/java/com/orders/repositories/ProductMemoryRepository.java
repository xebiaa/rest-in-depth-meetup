package com.orders.repositories;

import com.orders.domain.Product;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by jan on 22/11/14.
 */
@Component
public class ProductMemoryRepository implements ProductRepository {


    private Map<UUID, Product> products = Collections.unmodifiableMap(new HashMap<UUID, Product>());

    @Override
    public synchronized Product save(Product product) {

        Map<UUID, Product> modifiableProducts = new HashMap<UUID, Product>(products);
        modifiableProducts.put(product.getIdentifier(), product);
        this.products = Collections.unmodifiableMap(modifiableProducts);

        return product;
    }

    @Override
    public synchronized boolean delete(UUID key) {
        boolean deleted = false;
        if (products.containsKey(key)) {
            Map<UUID, Product> modifiableProducts = new HashMap<UUID, Product>(products);
            modifiableProducts.remove(key);
            this.products = Collections.unmodifiableMap(modifiableProducts);
            deleted = true;
        }
        return deleted;
    }

    @Override
    public Product findById(UUID key) {
        return products.get(key);
    }

    @Override
    public List<Product> findAll() {
        return Collections.unmodifiableList(new ArrayList<Product>(products.values()));
    }

}
