package com.orders.repositories;

import com.orders.domain.Product;

import java.util.List;
import java.util.UUID;

/**
 * Created by jan on 22/11/14.
 */


public interface ProductRepository {

    Product save(Product order);

    boolean delete(UUID key);

    Product findById(UUID key);

    List<Product> findAll();
}

