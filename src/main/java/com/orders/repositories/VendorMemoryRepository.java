package com.orders.repositories;

import com.orders.domain.Vendor;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by marco on 03/11/14.
 */
@Component
public class VendorMemoryRepository implements VendorRepository {

    private Map<UUID, Vendor> vendors = Collections.unmodifiableMap(new HashMap<UUID, Vendor>());

    @Override
    public synchronized Vendor save(Vendor vendor) {

        Map<UUID, Vendor> modifiableOrders = new HashMap<UUID, Vendor>(vendors);
        modifiableOrders.put(vendor.getId(), vendor);
        this.vendors = Collections.unmodifiableMap(modifiableOrders);

        return vendor;
    }

    @Override
    public synchronized boolean delete(UUID key) {
        boolean deleted = false;
        if (vendors.containsKey(key)) {
            Map<UUID, Vendor> modifiableOrders = new HashMap<UUID, Vendor>(vendors);
            modifiableOrders.remove(key);
            this.vendors = Collections.unmodifiableMap(modifiableOrders);
            deleted = true;
        }
        return deleted;
    }

    @Override
    public Vendor findById(UUID key) {
        return vendors.get(key);
    }

    @Override
    public List<Vendor> findAll() {
        return Collections.unmodifiableList(new ArrayList<Vendor>(vendors.values()));
    }

}
