package com.orders.repositories;

import com.orders.domain.Vendor;

import java.util.List;
import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */


public interface VendorRepository {

    Vendor save(Vendor order);

    boolean delete(UUID key);

    Vendor findById(UUID key);

    List<Vendor> findAll();

}

