package com.orders;

import com.orders.domain.Order;
import com.orders.domain.OrderItem;
import com.orders.domain.Product;
import com.orders.domain.Vendor;
import com.orders.repositories.OrderMemoryRepository;
import com.orders.repositories.ProductMemoryRepository;
import com.orders.repositories.VendorMemoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.UriTemplate;
import org.springframework.hateoas.hal.CurieProvider;
import org.springframework.hateoas.hal.DefaultCurieProvider;

import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class Application {

    @Bean
    public CurieProvider curieProvider() {
        return new DefaultCurieProvider("mo",
                new UriTemplate("http://myorder.com/relations/{rel}"));
    }

    @Autowired
    private OrderMemoryRepository orderRepository;
    @Autowired
    private ProductMemoryRepository productRepository;

    @Autowired
    private VendorMemoryRepository vendorRepository;

    private void initReposWithTestData() {
        Vendor vendor = new Vendor("Vendor1");
        vendorRepository.save(vendor);
        Product product = new Product(UUID.randomUUID(),"product 1", vendor);
        productRepository.save(product);
        Order order = new Order(UUID.randomUUID(), "testuser1");
        OrderItem item1 = new OrderItem(UUID.randomUUID(),42, product.getIdentifier(), order.getId());
        OrderItem item2 = new OrderItem(UUID.randomUUID(),48, product.getIdentifier(), order.getId());
        order.setOrderItem(item1);
        order.setOrderItem(item2);
        orderRepository.save(order);
    }

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        Application application = ctx.getBean(Application.class);
        application.initReposWithTestData();
    }
}
