package com.orders.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */
public class Order {

    public enum Status {

        NEW, APPROVED
    }

    private Map orderItems = new HashMap<UUID, OrderItem>();
    private String accountId;
    private UUID id;
    private Status status;

    public Order(UUID id, String accountId) {
        this.id = id;
        this.accountId = accountId;
        this.status = Status.NEW;
    }

    public Order(String accountId) {
        this.id = UUID.randomUUID();
        this.accountId = accountId;
        this.status = Status.NEW;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Map getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Map orderItems) {
        this.orderItems = orderItems;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItems.put(orderItem.getId(), orderItem);
    }

    public void removeOrderItems(){
        this.orderItems.clear();
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (accountId != null ? !accountId.equals(order.accountId) : order.accountId != null) return false;
        if (id != null ? !id.equals(order.id) : order.id != null) return false;
        if (orderItems != null ? !orderItems.equals(order.orderItems) : order.orderItems != null) return false;
        if (status != order.status) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderItems != null ? orderItems.hashCode() : 0;
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "accountId='" + accountId + '\'' +
                ", id=" + id +
                ", status=" + status +
                '}';
    }
}
