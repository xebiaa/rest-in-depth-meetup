package com.orders.domain;

import java.util.UUID;

/**
 * Created by jan on 22/11/14.
 */
public class Product {

    private UUID identifier;
    private String name;
    private Vendor vendor;

    public Product(UUID identifier, String name, Vendor vendor) {
        this.identifier = identifier;
        this.name = name;
        this.vendor = vendor;
    }
    public Product(String name) {
        this.identifier = UUID.randomUUID();
        this.name = name;
    }

    public UUID getIdentifier() {
        return identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @Override
    public String toString() {
        return "Product{" +
                "identifier=" + identifier +
                ", name='" + name + '\'' +
                ", vendor='" + vendor + '\'' +
                '}';
    }
}
