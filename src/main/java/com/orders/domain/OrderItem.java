package com.orders.domain;

import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */
public class OrderItem {

    private UUID id;
    private Integer amount;
    private UUID productId;
    private UUID parentId;

    public OrderItem(UUID identifier, Integer amount, UUID productId, UUID parentId) {
        this.id = identifier;
        this.amount = amount;
        this.productId = productId;
        this.parentId = parentId;
    }

    public OrderItem(Integer amount, UUID productId, UUID parentId) {
        this.id = UUID.randomUUID();
        this.amount = amount;
        this.productId = productId;
        this.parentId = parentId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public UUID getProductId() {
        return productId;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public void setProductId(UUID productId) {
        this.productId = productId;
    }

    public UUID getParentId() {
        return parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderItem orderItem = (OrderItem) o;

        if (amount != null ? !amount.equals(orderItem.amount) : orderItem.amount != null) return false;
        if (id != null ? !id.equals(orderItem.id) : orderItem.id != null) return false;
        if (parentId != null ? !parentId.equals(orderItem.parentId) : orderItem.parentId != null) return false;
        if (productId != null ? !productId.equals(orderItem.productId) : orderItem.productId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "id=" + id +
                ", amount=" + amount +
                ", productId=" + productId +
                ", parentId=" + parentId +
                '}';
    }
}
