package com.orders.rest.resources;

import com.orders.domain.Product;
import com.orders.domain.Vendor;
import com.orders.rest.controllers.ProductController;
import com.orders.rest.controllers.VendorController;
import com.orders.rest.resources.ProductResource;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 * Created by jan on 22/11/14.
 */
public class ProductResourceAssembler extends ResourceAssemblerSupport<Product, ProductResource> {

    private EntityLinks entityLinks;

    public ProductResourceAssembler() {
        super(ProductController.class, ProductResource.class);
    }

    public ProductResourceAssembler(EntityLinks el) {
        super(ProductController.class, ProductResource.class);
        entityLinks = el;
    }

    @Override
    public ProductResource toResource(Product product) {

        ProductResource resource = createResourceWithId(product.getIdentifier(), product);
        BeanUtils.copyProperties(product, resource);

        // TODO: add link to vendor using ControllerLinkBuilder LinkTo

        // TODO: add link to vendor using LinkTo and MethodOn
        resource.add(linkTo(methodOn(VendorController.class).viewVendor(product.getVendor().getId().toString())).withRel("vendor2"));

        // TODO: add link to vendor using entityLinks

        return resource;

    }

}
