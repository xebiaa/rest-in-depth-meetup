package com.orders.rest.resources;

import com.orders.domain.Vendor;

/**
 * Created by marco on 03/11/14.
 */

public class VendorResource extends HALResource {

    public String name;

    public VendorResource(String name) {
        this.name = name;
    }

    public VendorResource() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
