package com.orders.rest.resources;

import com.orders.domain.Order;
import com.orders.domain.OrderItem;
import com.orders.domain.Product;
import com.orders.rest.controllers.OrderController;
import com.orders.rest.controllers.OrderItemController;
import com.orders.rest.controllers.ProductController;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by marco on 06/11/14.
 */
public class OrderItemResourceAssembler extends ResourceAssemblerSupport<OrderItem, OrderItemResource> {

    public static final String REL_NAME_PRODUCT = "product";
    public static final String REL_NAME_ORDER = "order";

    private EntityLinks entityLinks;

    public OrderItemResourceAssembler() {
        super(OrderItemController.class, OrderItemResource.class);
    }

    public OrderItemResourceAssembler(EntityLinks el) {
        super(OrderItemController.class, OrderItemResource.class);
        entityLinks = el;
    }

    @Override
    public OrderItemResource toResource(OrderItem orderItem) {

        //OrderItemResource resource = createResourceWithId(orderItem.getId(), orderItem);  // this does not generate the correct self url
        OrderItemResource resource = instantiateResource(orderItem);
        resource.add(linkTo(methodOn(OrderController.class).viewOrder(orderItem.getParentId().toString())).slash("items").slash(orderItem.getId()).withSelfRel());


        // creating link to Product using methodOn()
        //resource.add(linkTo(methodOn(ProductController.class).viewProduct(orderItem.getProductId().toString())).withRel(REL_NAME_PRODUCT));

        // creating link to Product using entityLinks
        resource.add(entityLinks.linkToSingleResource(Product.class, orderItem.getProductId()).withRel(REL_NAME_PRODUCT));

        // create link to parent order
        resource.add(entityLinks.linkToSingleResource(Order.class, orderItem.getParentId()).withRel(REL_NAME_ORDER));


        return resource;
    }

    @Override
    protected OrderItemResource instantiateResource(OrderItem orderItem) {
        return new OrderItemResource(orderItem);
    }

}
