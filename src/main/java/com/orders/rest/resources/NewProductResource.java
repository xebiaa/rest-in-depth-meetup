package com.orders.rest.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * Created by marco on 12/12/14.
 */
public class NewProductResource {

    private String name;

    @JsonCreator
    public NewProductResource(@JsonProperty(value = "name") @NotNull String name) {

        this.name = name;
    }

    public NewProductResource() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
