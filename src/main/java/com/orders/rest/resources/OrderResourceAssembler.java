package com.orders.rest.resources;

import com.orders.domain.Order;
import com.orders.domain.OrderItem;
import com.orders.rest.controllers.OrderController;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.UriTemplate;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.util.*;

/**
 * Created by marco on 06/11/14.
 */
public class OrderResourceAssembler extends ResourceAssemblerSupport<Order, OrderResource> {

    public static final String REL_NAME_ITEMS = "items";
    public static final String REL_NAME_APPROVE = "approve";
    public static final String REL_NAME_SEARCH = "approved_orders";


    private EntityLinks entityLinks;

    public OrderResourceAssembler() {
        super(OrderController.class, OrderResource.class);
    }

    public OrderResourceAssembler(EntityLinks el) {
        super(OrderController.class, OrderResource.class);
        entityLinks = el;
    }

    @Override
    public OrderResource toResource(Order order) {

        OrderResource resource = createResourceWithId(order.getId(), order);
        Iterator<OrderItem> it = order.getOrderItems().values().iterator();

        // TODO: Need to fix. This iteration does not work. It overwrites. See HALResource.
        while (it.hasNext()) {
            OrderItem item = it.next();
            resource.embedResource("items", new OrderItemResourceAssembler(entityLinks).toResource(item));

        }

        // creating link to Approve using methodOn()
        // resource.add(linkTo(methodOn(OrderController.class).approveOrder(order.getIdentifier().toString(), "")).withRel(OrderResource.REL_NAME_APPROVE));

        // creating same link for approve using slash() instead of methodOn()
        resource.add(linkTo(OrderController.class).slash(order.getId().toString()).slash("approve").withRel(REL_NAME_APPROVE));

        // creating link to OrderItems
        resource.add(linkTo(methodOn(OrderController.class).getAllOrderItems(order.getId().toString())).withRel(REL_NAME_ITEMS));

        // creating link for search
        resource.add(linkTo(methodOn(OrderController.class).searchApprovedOrders()).withRel(REL_NAME_SEARCH));

        // link to test entitylinks
        resource.add(entityLinks.linkFor(Order.class).slash(order.getAccountId().toString()).withRel("account"));
        //resource.add(entityLinks.linkToSingleResource(Order.class, order.getAccountId().toString()).withRel("account"));

        return resource;

    }

    @Override
    protected OrderResource instantiateResource(Order order) {
        return new OrderResource(order);
    }

}
