package com.orders.rest.resources;

import com.orders.domain.Vendor;
import com.orders.rest.controllers.VendorController;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by jan on 22/11/14.
 */
public class VendorResourceAssembler extends ResourceAssemblerSupport<Vendor, VendorResource> {

    private EntityLinks entityLinks;

    public VendorResourceAssembler() {
        super(VendorController.class, VendorResource.class);
    }

    public VendorResourceAssembler(EntityLinks el) {
        super(VendorController.class, VendorResource.class);
        entityLinks = el;
    }

    @Override
    public VendorResource toResource(Vendor vendor) {

        VendorResource resource = createResourceWithId(vendor.getId(), vendor);
        BeanUtils.copyProperties(vendor, resource);
        return resource;
    }

}
