package com.orders.rest.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */

public class NewOrderResource {


    private Integer amount;
    private UUID productId;

    @JsonCreator
    public NewOrderResource(@NotNull @JsonProperty("amount") Integer amount, @NotNull @JsonProperty("productId") UUID productId) {

        this.amount = amount;
        this.productId = productId;
    }

    public Integer getAmount() {
        return amount;
    }

    public UUID getProductId() {
        return productId;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public void setProductId(UUID productId) {
        this.productId = productId;
    }

}
