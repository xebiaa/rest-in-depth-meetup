package com.orders.rest.resources;

import com.orders.domain.Order;
import com.orders.domain.OrderItem;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by marco on 03/11/14.
 */

public class OrderItemResource extends HALResource {


    public OrderItem orderItem;

    public OrderItemResource(OrderItem oi) {

        orderItem = oi;
    }

    public OrderItemResource() {

    }

}
