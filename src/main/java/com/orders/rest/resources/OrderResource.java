package com.orders.rest.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.orders.domain.Order;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.Identifiable;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */

public class OrderResource extends HALResource {

   // private String accountId;
   // private String status;

    private class ResourceOrder{

        public String accountId;
        public String status;

        public ResourceOrder(String accId, String sts){
            accountId = accId;
            status = sts;
        }

    }
    public ResourceOrder order;

    public OrderResource(Order o) {
        order = new ResourceOrder(o.getAccountId(), o.getStatus().toString());
    }

    public OrderResource() {

    }


}
