package com.orders.rest.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * Created by jan on 22/11/14.
 */

public class ProductResource extends ResourceSupport {

    private UUID identifier;
    private String name;

    @JsonCreator
    public ProductResource(@JsonProperty(value = "identifier") @NotNull UUID identifier, @JsonProperty(value = "amount") @NotNull String name) {

        this.identifier = identifier;
        this.name = name;
    }

    public ProductResource() {
    }

    public String getName() {
        return name;
    }

    public UUID getIdentifier() { return identifier; }
    public void setIdentifier(UUID identifier) {
        this.identifier = identifier;
    }

    public void setName(String name) {
        this.name = name;
    }

}
