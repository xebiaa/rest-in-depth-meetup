package com.orders.rest.controllers;

import com.orders.domain.Order;
import com.orders.domain.OrderItem;
import com.orders.repositories.OrderMemoryRepository;
import com.orders.rest.resources.OrderItemResource;
import com.orders.rest.resources.OrderItemResourceAssembler;
import com.orders.rest.resources.OrderResource;
import com.orders.rest.resources.OrderResourceAssembler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */

@RestController
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)    // HAL is selected by default by Spring HATEAOS
@ExposesResourceFor(OrderItem.class)
@RequestMapping("api/NOT_USED")
public class OrderItemController {

    private static Logger LOG = LoggerFactory.getLogger(OrderItemController.class);

    @Autowired
    private OrderMemoryRepository orderRepository;

    @Autowired
    private EntityLinks entityLinks;


    /**
     * GET OrderItems per Order
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/hal+json")
    public ResponseEntity<List<OrderItemResource>> getAllOrderItems(@PathVariable String id) {

        Order order = orderRepository.findById(UUID.fromString(id));

        if (order == null || order.getOrderItems().isEmpty()) {
            return new ResponseEntity<List<OrderItemResource>>(HttpStatus.NOT_FOUND);
        }

        List<OrderItemResource> orderItemResources = new OrderItemResourceAssembler(entityLinks).
                toResources(order.getOrderItems().entrySet());

        return new ResponseEntity<List<OrderItemResource>>(orderItemResources, HttpStatus.OK);
    }

}
