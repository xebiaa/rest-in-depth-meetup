package com.orders.rest.controllers;

import com.orders.domain.Product;
import com.orders.repositories.ProductMemoryRepository;
import com.orders.rest.resources.ProductResource;
import com.orders.rest.resources.ProductResourceAssembler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jan on 22/11/14.
 */
@RestController
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
@ExposesResourceFor(Product.class)
@RequestMapping("/api/products")
public class ProductController {

    private static Logger LOG = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductMemoryRepository productRepository;

    @Autowired
    private EntityLinks entityLinks;


    @RequestMapping(method = RequestMethod.GET)
    public List<ProductResource> getAllProducts() {
        List<ProductResource> productResources = new ArrayList<ProductResource>();

        productResources = new ProductResourceAssembler(entityLinks).toResources(productRepository.findAll());

        return productResources;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<ProductResource> viewProduct(@PathVariable String id) {

        Product product = productRepository.findById(UUID.fromString(id));

        if (product == null) {
            return new ResponseEntity<ProductResource>(HttpStatus.NOT_FOUND);
        }
        ProductResource resource = new ProductResourceAssembler(entityLinks).toResource(product);

        return new ResponseEntity<ProductResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ProductResource> createProduct(@RequestBody ProductResource productResource) {

        Product responseProduct = productRepository.save(new Product(productResource.getName()));
        ProductResource responseResource = new ProductResourceAssembler(entityLinks).toResource(responseProduct);

        return new ResponseEntity<ProductResource>(responseResource, HttpStatus.CREATED);
    }

    // TODO: Add removeProduct
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<ProductResource> cancelProduct(@PathVariable String id) {

        boolean deleted = productRepository.delete(UUID.fromString(id));
        if (!deleted) {
            return new ResponseEntity<ProductResource>(HttpStatus.NOT_FOUND);
        } else

        {
            return new ResponseEntity<ProductResource>(HttpStatus.OK);
        }
    }

    }
