package com.orders.rest.controllers;

import com.orders.rest.resources.HALResource;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;


/**
 * Created by jan on 21/11/14.
 */
@RestController
@RequestMapping(value = "/api")
public class SiteMapController {

    @RequestMapping(method = RequestMethod.GET, produces = "application/hal+json")
    public SiteMap siteMap() {

        SiteMap sitemap = new SiteMap();
        sitemap.add(linkTo(SiteMapController.class).withSelfRel());
        sitemap.add(new Link(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/orders").build().toUriString()).withRel("orders"));
        sitemap.add(new Link(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/products").build().toUriString()).withRel("products"));
        return sitemap;
    }

    private class SiteMap extends HALResource {
        public String sitemap = "This is the sitemap for the myorders API";
    }
}
