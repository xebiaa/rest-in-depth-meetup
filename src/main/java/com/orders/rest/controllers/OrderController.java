package com.orders.rest.controllers;

import com.orders.domain.Order;
import com.orders.domain.OrderItem;
import com.orders.repositories.OrderMemoryRepository;
import com.orders.rest.resources.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.util.List;
import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */

/* TODO: Add pagination to the GET /orders and include prev, next rel's to resource. Use PagedResources from Spring HATEAOS

*/
@RestController
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)    // HAL is selected by default by Spring HATEAOS
@ExposesResourceFor(Order.class)
@RequestMapping("/api/orders")
public class OrderController {

    private static Logger LOG = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderMemoryRepository orderRepository;

    @Autowired
    private EntityLinks entityLinks;

    /**
     * GET Orders
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/hal+json")
    public ResponseEntity<List<OrderResource>> getAllOrders() {

        List<OrderResource> orderResources = new OrderResourceAssembler(entityLinks).
                toResources(orderRepository.findAll());

        return new ResponseEntity<List<OrderResource>>(orderResources, HttpStatus.OK);
    }

    /**
     * GET Order by id
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/hal+json", value = "/{id}")
    public ResponseEntity<OrderResource> viewOrder(@PathVariable String id) {

        Order order = orderRepository.findById(UUID.fromString(id));

        if (order == null) {
            return new ResponseEntity<OrderResource>(HttpStatus.NOT_FOUND);
        }
        OrderResource resource = new OrderResourceAssembler(entityLinks).toResource(order);

        HttpHeaders headers = new HttpHeaders();
        headers.add("ETag", Integer.toString(order.hashCode()));

        return new ResponseEntity<OrderResource>(resource, headers, HttpStatus.OK);
    }

    /**
     * POST NewOrder
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/hal+json")
    public ResponseEntity<OrderResource> createOrder(@RequestBody NewOrderResource orderResource) {

        Order order = new Order("testUser");
        order.setOrderItem(new OrderItem(orderResource.getAmount(), orderResource.getProductId(), order.getId()));
        Order responseOrder = orderRepository.save(order);
        OrderResource responseResource = new OrderResourceAssembler(entityLinks).toResource(responseOrder);
        HttpHeaders headers = new HttpHeaders();
        headers.add("ETag", Integer.toString(responseOrder.hashCode()));
        headers.setLocation(linkTo(methodOn(getClass()).viewOrder(responseOrder.getId().toString())).toUri());
        return new ResponseEntity<OrderResource>(responseResource, headers, HttpStatus.CREATED);
    }

    /**
     * DELETE Order by id
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<OrderResource> cancelOrder(@PathVariable String id, @RequestHeader(value="If-Match") String eTag) {

        boolean deleted = false;

        Order order = orderRepository.findById(UUID.fromString(id));
        if (order == null ){
            return new ResponseEntity<OrderResource>(HttpStatus.NOT_FOUND);
        }
        if (eTag.equals("")){
            return new ResponseEntity<OrderResource>(HttpStatus.BAD_REQUEST);
        }
        String et = Integer.toString(order.hashCode());
        if (eTag.equals(et)) {
            deleted = orderRepository.delete(UUID.fromString(id));
        }
        if (!deleted) {
            return new ResponseEntity<OrderResource>(HttpStatus.PRECONDITION_FAILED);
        } else {
            return new ResponseEntity<OrderResource>(HttpStatus.NO_CONTENT);
        }

    }

    /**
     * PUT change to existing Order is not allowed
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/hal+json")
    public ResponseEntity<OrderResource> updateOrder(@PathVariable String id) {

        return new ResponseEntity<OrderResource>(HttpStatus.METHOD_NOT_ALLOWED);

    }

    /**
     * PUT change to existing Order status
     */
    @RequestMapping(value = "/{orderId}/approve", method = RequestMethod.PUT, produces = "application/hal+json")
    public ResponseEntity<OrderResource> approveOrder(@PathVariable String orderId, @RequestHeader(value="If-Match") String eTag) {


        boolean approved = false;

        Order order = orderRepository.findById(UUID.fromString(orderId));
        if (order == null ){
            return new ResponseEntity<OrderResource>(HttpStatus.NOT_FOUND);
        }
        if (eTag.equals("")){
            return new ResponseEntity<OrderResource>(HttpStatus.PRECONDITION_REQUIRED);
        }
        String et = Integer.toString(order.hashCode());
        if (eTag.equals(et)) {
            approved = orderRepository.approve(UUID.fromString(orderId));
        }
        if (!approved) {
            return new ResponseEntity<OrderResource>(HttpStatus.PRECONDITION_FAILED);
        } else {
            return new ResponseEntity<OrderResource>(HttpStatus.ACCEPTED);
        }
    }

    /**
     * GET OrderItems per Order
     */
    @RequestMapping(value="/{id}/orderItems", method = RequestMethod.GET, produces = "application/hal+json")
    public ResponseEntity<List<OrderItemResource>> getAllOrderItems(@PathVariable String id) {

        Order order = orderRepository.findById(UUID.fromString(id));

        if (order == null || order.getOrderItems().isEmpty()) {
            return new ResponseEntity<List<OrderItemResource>>(HttpStatus.NOT_FOUND);
        }

        List<OrderItemResource> orderItemResources = new OrderItemResourceAssembler(entityLinks).
                toResources(order.getOrderItems().values());

        return new ResponseEntity<List<OrderItemResource>>(orderItemResources, HttpStatus.OK);
    }

    /**
     * GET OrderItem
     */
    @RequestMapping(value="/{orderId}/items/{itemId}", method = RequestMethod.GET, produces = "application/hal+json")
    public ResponseEntity<OrderItemResource> getOrderItem(@PathVariable String orderId, @PathVariable String itemId) {

        Order order = orderRepository.findById(UUID.fromString(orderId));

        if (order == null || order.getOrderItems().isEmpty() || !(order.getOrderItems().containsKey(UUID.fromString(itemId)))) {
            return new ResponseEntity<OrderItemResource>(HttpStatus.NOT_FOUND);
        }

        OrderItemResource orderItemResource = new OrderItemResourceAssembler(entityLinks).
                toResource((OrderItem) order.getOrderItems().get(UUID.fromString(itemId)));

        return new ResponseEntity<OrderItemResource>(orderItemResource, HttpStatus.OK);
    }


    /**
     * GET Search Orders by field
     * TODO: Does not actually implement only returning approved orders, just returns all orders.
     */
    @RequestMapping(value="/search/approved", method = RequestMethod.GET, produces = "application/hal+json")
    public ResponseEntity<List<OrderResource>> searchApprovedOrders() {

        List<OrderResource> orderResources = new OrderResourceAssembler(entityLinks).
                toResources(orderRepository.findAll());

        return new ResponseEntity<List<OrderResource>>(orderResources, HttpStatus.OK);
    }

}
