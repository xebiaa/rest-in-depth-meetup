package com.orders.rest.controllers;

import com.orders.domain.Order;
import com.orders.domain.OrderItem;
import com.orders.domain.Vendor;
import com.orders.repositories.OrderMemoryRepository;
import com.orders.repositories.VendorMemoryRepository;
import com.orders.rest.resources.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by marco on 03/11/14.
 */

@RestController
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)    // HAL is selected by default by Spring HATEAOS
@ExposesResourceFor(Vendor.class)
@RequestMapping("/api/vendors")
public class VendorController {

    private static Logger LOG = LoggerFactory.getLogger(VendorController.class);

    @Autowired
    private VendorMemoryRepository vendorRepository;

    @Autowired
    private EntityLinks entityLinks;

    /**
     * GET Vendors
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/hal+json")
    public ResponseEntity<List<VendorResource>> getAllVendors() {

        List<VendorResource> vendorResources = new VendorResourceAssembler(entityLinks).
                toResources(vendorRepository.findAll());

        return new ResponseEntity<List<VendorResource>>(vendorResources, HttpStatus.OK);
    }

    /**
     * GET Vendor by id
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/hal+json", value = "/{id}")
    public ResponseEntity<VendorResource> viewVendor(@PathVariable String id) {

        Vendor vendor = vendorRepository.findById(UUID.fromString(id));

        if (vendor == null) {
            return new ResponseEntity<VendorResource>(HttpStatus.NOT_FOUND);
        }
        VendorResource resource = new VendorResourceAssembler(entityLinks).toResource(vendor);

        HttpHeaders headers = new HttpHeaders();
        headers.add("ETag", Integer.toString(vendor.hashCode()));

        return new ResponseEntity<VendorResource>(resource, headers, HttpStatus.OK);
    }
}
