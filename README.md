RestInDepthMeetup
=================

Sources for the Rest in Depth meetup. Goal is to explore Web API’s using Spring Boot and Spring HATEOAS.
The secondary goal is to see how Atom protocol can be used to integrate applications.
   
Orders API with a REST/HATEOAS web interface, implemented using Spring HATEAOS.
Products API. This is to be expanded / implemented during the Meetup.

Optional topics:
* Client implementation for HATEOAS
* Client implementation for Atom
* Automatically discoverable API's

Known bugs & limitations:
* embedding resources in Spring HATEAOS requires custom coding
* not possible to link to resources outside of the classpath

References and interesting links:
* RESTful Web APIs book: http://shop.oreilly.com/product/0636920028468.do
* REST in Practice book: http://restinpractice.com/book/
* Spring HATEAOS on GitHub: https://github.com/spring-projects/spring-hateoas
* Spring guide: http://spring.io/guides/gs/rest-service/
* Spring restbucks implementation: https://github.com/olivergierke/spring-restbucks
* Series on Web API's on InfoQ: http://www.infoq.com/articles/Web-APIs-From-Start-to-Finish
* Web API best practices: http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api
* About HAL: http://stateless.co/hal_specification.html and http://tools.ietf.org/html/draft-kelly-json-hal-06
* Framework comparison: http://zeroturnaround.com/rebellabs/beyond-rest-how-to-build-a-hateoas-api-in-java-with-spring-mvc-jersey-jax-rs-and-vraptor/
* Hydra Web API: http://www.markus-lanthaler.com/hydra/
* Blog on hypermedia formats: http://www.foxycart.com/blog/the-hypermedia-debate#.VHOUZNaz5nk
* Use OPTION or Allow header to discover options for a link: http://stackoverflow.com/questions/9204110/restful-api-runtime-discoverability-hateoas-client-design
* Example of a HAL based API: https://api.foxycart.com/docs
* The HAL Browser: http://haltalk.herokuapp.com/explorer/browser.html#/
